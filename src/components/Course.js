// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { useState, useEffect } from 'react';

const Course = (props) => {
	let course = props.course;

	const [isDisabled, setIsDisabled] = useState(false)
	const [seats, setSeats] = useState(30)

	useEffect(()=>{
		if(seats === 0){
			setIsDisabled(true)
		}
	},[seats])

	return(
		<Card>
  			<Card.Body>
    			<Card.Title>{course.name} </Card.Title>
    			<h6>Desciption:</h6>
      			<p> {course.description} </p>
    			<h6>Price:</h6>
    			<p>{course.price}</p>
    			<h6>Seats</h6>
    			<p>{seats} remaining</p>
    			<Button variant="primary" onClick ={()=> setSeats(seats-1)} disabled={isDisabled}> Enroll </Button>
  			</Card.Body>
		</Card>
	)
}



export default Course