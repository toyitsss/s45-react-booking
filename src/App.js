// Base Imports
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './components/PageNotFound';


const App = () => {
	return (
		<BrowserRouter>
        <AppNavBar />
        <Routes>
            <Route path ="/" element ={<Home />} />
            <Route path = "/courses" element ={<Courses />} />
            <Route path ="/register" element ={<Register />} />
            <Route path ="/login" element ={<Login />} />
            <Route path ="*" element ={<PageNotFound />} />
        </Routes>
        </BrowserRouter>
	)
}


export default App